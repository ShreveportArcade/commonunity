# Shreveport Common LINK - Unity3D project files#

### About ###
Shreveport Common LINK is a community driven 3D tour of the culture, history, architecture, and arts of nine-blocks on the west side of downtown Shreveport, Louisiana.

This is the client. It displays 3D models of structures in downtown Shreveport and media served from a Rails App.

### Contribution guidelines ###

If you get the urge to make a 3D model of one of the buildings in Downtown Shreveport, we ask that you do the following:

* Use the scale one meter to one unit.
* Use no more than 2 materials.
* Try to keep your triangle count under 2000.
* [Use Unity's LOD naming convention](https://docs.unity3d.com/Manual/LevelOfDetail.html)
* Export your textures as PNG (.png) files.
* Export your model as a COLLADA (.dae) file.