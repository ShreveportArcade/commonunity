﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UserLocationController : MonoBehaviour {

	public static UserLocationController instance;

	public delegate void OnUserMoved ();
	public static event OnUserMoved onUserMoved = delegate {};

	public Transform billboard;
	public float minDist = 100;
	public float maxDist = 1000;
	public float farSize = 10;
	public float nearSize = 1;

	void OnEnable () {
		MapCameraController.onCameraMoved += OnCameraMoved;
		LocationManager.onLocationChanged += LatLonChanged;

	}

	void OnDisable () {
		MapCameraController.onCameraMoved -= OnCameraMoved;
		LocationManager.onLocationChanged -= LatLonChanged;
	}

	void Awake () {
		if (instance == null) {
			instance = this;
		}
	}

	void OnCameraMoved (float dist) {
		float frac = (Mathf.Clamp(dist, minDist, maxDist) - minDist) / (maxDist - minDist);
		transform.localScale = Vector3.one * Mathf.Lerp(nearSize, farSize, frac);
		billboard.rotation = Quaternion.LookRotation(Camera.main.transform.forward, Camera.main.transform.up);
	}

	void LatLonChanged (double lat, double lon) {
		Vector3 p = LocationsUIController.LatLonToPosition(lat, lon) + Vector3.up * 0.1f;
		if ((p - transform.position).sqrMagnitude > 0.1f) {
			transform.position = p;
			onUserMoved();
		}
	}
}
