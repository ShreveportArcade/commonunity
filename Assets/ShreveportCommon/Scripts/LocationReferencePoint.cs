﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocationReferencePoint : MonoBehaviour {

	public double latitude;
	public double longitude;
}