﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocationManager : MonoBehaviour {

	public delegate void OnLocationStatusChange ();
	public static event OnLocationStatusChange onServiceEnabled = delegate {};
	public static event OnLocationStatusChange onServiceDisabled = delegate {};

	public delegate void OnLocationChanged (double lat, double lon);
	public static event OnLocationChanged onLocationChanged = delegate {};

	public static LocationManager instance;

	public float updateInterval = 1;

	public double _lat = 32.5088117942136;
	public static double lat {
		get { 
			if (instance == null) return 32.5088117942136;
			return instance._lat; 
		}
	}

	public double _lon = -93.7512555252114;
	public static double lon {
		get { 
			if (instance == null) return -93.7512555252114;
			return instance._lon; 
		}
	}

	public static bool locationEnabled {
		get { 
			return SystemInfo.supportsLocationService && 
				Input.location.isEnabledByUser && 
				Input.location.status == LocationServiceStatus.Running;
		}
	}

	void Awake () {
		if (instance == null) instance = this;
	}

	void OnEnable () {
		RequestManager.onUserLocationRetrieved += LocationRetrievedFromIP;
	}

	void OnDisable () {
		RequestManager.onUserLocationRetrieved -= LocationRetrievedFromIP;
		onServiceDisabled();
	}

	void Start () {
		StartCoroutine("UpdateLocation");
		onLocationChanged(_lat, _lon);
	}
	
	private LocationServiceStatus _locationStatus = LocationServiceStatus.Stopped;
	private LocationServiceStatus locationStatus {
		get { return _locationStatus; }
		set {
			if (_locationStatus != value) {
				_locationStatus = value;
				switch (_locationStatus) {
					case LocationServiceStatus.Running:
						onServiceEnabled();
						break;

					case LocationServiceStatus.Failed:
						onServiceDisabled();
						break;

					default:
						break;
				}
			}
		}
	}
	IEnumerator UpdateLocation () {
		if (SystemInfo.supportsLocationService && !Input.location.isEnabledByUser) {
			// prompt users to turn on GPS
		}

		while (enabled && SystemInfo.supportsLocationService) {
			yield return new WaitForSeconds(updateInterval);

			if (Input.location.isEnabledByUser) {
				locationStatus = Input.location.status;
				switch (locationStatus) {
					case LocationServiceStatus.Running:
						_lat = Input.location.lastData.latitude;
						_lon = Input.location.lastData.longitude;
						onLocationChanged(_lat, _lon);
						break;

					case LocationServiceStatus.Stopped:
						Input.location.Start();
						break;

					default:
						break;
				}
			}
		}

		// if (!SystemInfo.supportsLocationService) {
		// 	RequestManager.GetUserLocationFromIP();
		// }
	}

	void LocationRetrievedFromIP (double latitude, double longitude) {
		_lat = latitude;
		_lon = longitude;
		onLocationChanged(_lat, _lon);
	}
}
