﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MapCameraController : MonoBehaviour {

	public delegate void OnCameraMoved(float distance);
	public static event OnCameraMoved onCameraMoved = delegate {};

	public static MapCameraController instance;

	public enum CameraMode {
		BirdsEye,
		FirstPerson
	}
	public static CameraMode cameraMode = CameraMode.BirdsEye;

	public float speed = 10;
	public float minDist = 50;
	public float maxDist = 500;
	public float minAngle = -90;
	public float maxAngle = -45;

	public LayerMask raycastMask;
	public float dismissLookTargetTime = 2;
	
	new private Camera camera;
	private Plane groundPlane;
	private Vector3 startPos;
	private bool changed = true;
	private Ray camRay;
	private float camDist = 0;
	private Vector2 lastA;
	private Vector2 lastB;
	private bool wasMultitouching = false;
	private Vector3 lastCameraPosition;
	private Quaternion lastCameraRotation;
	private float lookTargetFoundTime;

	public float zoomDuration = 1;

	public static void ZoomInOnUser () {
		Vector3 forward = instance.transform.forward;
		forward.y = 0;
		forward = forward.normalized;

		LocationData c = LocationData.closest;
		if (c != null) {
			Vector3 p = LocationsUIController.LatLonToPosition(c.latitude, c.longitude) + Vector3.up * c.displayHeight;
			Vector3 target = (p + UserLocationController.instance.transform.position) * 0.5f;
			target -= forward * (p - UserLocationController.instance.transform.position).magnitude;
			ZoomIn(target);
		}
		else {
			forward.y = -1;
			forward *= instance.minDist;
			ZoomIn(UserLocationController.instance.transform.position - forward);
		}
	}

	public static void ZoomIn (Vector3 pos) {
		instance.StartCoroutine("ZoomInCoroutine", pos);
	}

	IEnumerator ZoomInCoroutine (Vector3 pos) {
		Vector3 startPos = transform.position;
		Vector3 startRot = transform.localEulerAngles;
		float pct = (pos.y - minDist) / (maxDist - minDist);
		Vector3 rot = transform.localEulerAngles;
		rot = new Vector3(Mathf.Lerp(minAngle, maxAngle, pct), rot.y, rot.z);
			
		for (float t = 0; t < zoomDuration; t += Time.deltaTime) {
			float frac = t / zoomDuration;
			transform.position = Vector3.Lerp(startPos, pos, frac);
			transform.localEulerAngles = Vector3.Lerp(startRot, rot, frac);
			onCameraMoved(Mathf.Lerp(camDist, transform.position.y, frac));
			yield return new WaitForEndOfFrame();
		}
		transform.position = pos;
		transform.localEulerAngles = rot;
		LocationsUIController.HideLabels();
	}

	void Awake () {
		if (instance == null) {
			instance = this;
			camera = GetComponentInChildren<Camera>();
			groundPlane = new Plane(Vector3.up, Vector3.zero);
		}
	}

	public static void SetBirdsEye () {
		cameraMode = CameraMode.BirdsEye;
		instance.transform.position = instance.lastCameraPosition;
		instance.transform.rotation = instance.lastCameraRotation;
		instance.camera.transform.localRotation = Quaternion.identity;
		Screen.sleepTimeout = SleepTimeout.SystemSetting;
		UserLocationController.instance.gameObject.SetActive(true);
	}

	public static void SetFirstPerson () {
		cameraMode = CameraMode.FirstPerson;
		instance.lastCameraPosition = instance.transform.position;
		instance.lastCameraRotation = instance.transform.rotation;
		instance.transform.position = Vector3.up;

        Screen.sleepTimeout = SleepTimeout.NeverSleep;
        LocationsUIController.HideLabels();
        UserLocationController.instance.gameObject.SetActive(false);
	}

	void Update () {
		if (MenuController.isObstructingView) return;

		switch (cameraMode) {
			case CameraMode.FirstPerson:
				FirstPersonUpdate();
				break;
			case CameraMode.BirdsEye:
				BirdsEyeUpdate();
				break;
		}
	}

	// HACK: This is how I found "true north" on a device.
	// private float angle = 180;
	// void OnGUI () {
	// 	angle = GUI.HorizontalSlider(new Rect(200,200,500,100), angle, 0, 360);
	// 	GUI.Label(new Rect(700,200,500,100), "" + angle);
	// }

	void FirstPersonUpdate () {
		camera.transform.rotation = Input.gyro.attitude;
		camera.transform.Rotate(0, 0, 180, Space.Self);
		camera.transform.Rotate(90, 180, 0, Space.World);
		
		transform.position = LocationsUIController.LatLonToPosition(LocationManager.lat, LocationManager.lon) + Vector3.up * 2;

		RaycastHit hit;
		if (Physics.SphereCast(camera.transform.position, 1, camera.transform.forward, out hit, 200, raycastMask)) {
			LocationLookTarget lookTarget = hit.collider.gameObject.GetComponent<LocationLookTarget>();
			MenuController.instance.currentLocation = lookTarget.location;
			lookTargetFoundTime = Time.time;
		}
		else if (Time.time - lookTargetFoundTime > dismissLookTargetTime) {
			MenuController.instance.currentLocation = null;
		}
	}

	void BirdsEyeUpdate () {
        camRay = Camera.main.ViewportPointToRay(Vector2.one * 0.5f);
		groundPlane.Raycast(camRay, out camDist);

		if (Input.touchCount < 2) {
			
			float panDist;
			Ray panRay = Camera.main.ScreenPointToRay(Input.mousePosition);
			if (Input.GetMouseButton(0) && groundPlane.Raycast(panRay, out panDist) && !MenuController.TouchedUI()) {
		        if (Input.GetMouseButtonDown(0) || wasMultitouching) {
		        	wasMultitouching = false;
		        	startPos = panRay.GetPoint(panDist);
		        }
		        else {
		        	Vector3 currentPos = panRay.GetPoint(panDist);
					Move(currentPos - startPos);
				}
			}

			Vector2 scroll = Input.mouseScrollDelta;
			if (Mathf.Abs(scroll.y) > Mathf.Abs(scroll.x)) {
				Zoom(scroll.y * speed * Time.deltaTime);
			}
			else {
				Rotate(scroll.x * speed * Time.deltaTime);
			}
		}
		else {
			wasMultitouching = true;
            Touch touchA = Input.GetTouch(0);
            Touch touchB = Input.GetTouch(1);
            
            if (touchB.phase == TouchPhase.Moved || touchB.phase == TouchPhase.Stationary) {
	            float lastDist = (lastA - lastB).magnitude;
	            float dist = (touchA.position - touchB.position).magnitude;
				Zoom((dist - lastDist) * speed * Time.deltaTime);

				Vector2 lastDiff = lastB - lastA;
				Vector2 diff = touchB.position - touchA.position;
				float lastAng = Mathf.Atan2(lastDiff.y, lastDiff.x) * Mathf.Rad2Deg;
				float ang = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
				Rotate(ang - lastAng);
			}

			lastA = touchA.position;
			lastB = touchB.position;
        }

		if (changed) {
			StopCoroutine("ZoomInCoroutine");
			onCameraMoved(camDist);
		}
		changed = false;
	}

	void Move (Vector3 change) {
		Vector3 originalPos = transform.position;
		transform.position -= change;

		Vector3 ne = LocationsUIController.instance.northEast.transform.position;
		Vector3 sw = LocationsUIController.instance.southWest.transform.position;
		Vector3 center = (ne + sw) * 0.5f;
		float r = Vector3.Distance(ne, center);

		float d;
		Ray ray = Camera.main.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
		groundPlane.Raycast(ray, out d);
		Vector3 newCamCenter = ray.GetPoint(d);
		float newDist = Vector3.Distance(center, newCamCenter);

		if (newDist > r && Vector3.Dot(change, newCamCenter - center) < 0) {
			transform.position = originalPos;
		}
		else {
			changed = true;
		}
	}

	void Zoom (float change) {
		if (change == 0) return;
		float newCamDist = Mathf.Clamp(camDist - change, minDist, maxDist);
		float pct = (newCamDist - minDist) / (maxDist - minDist);
		if (newCamDist != camDist) {
			changed = true;
			Vector3 rot = transform.localEulerAngles;
			transform.localEulerAngles = new Vector3(Mathf.Lerp(minAngle, maxAngle, pct), rot.y, rot.z);
			transform.position = camRay.GetPoint(camDist) - transform.forward * newCamDist;
		}
	}

	void Rotate (float change) {
		if (change == 0) return;
		changed = true;
		transform.RotateAround(camRay.GetPoint(camDist), Vector3.up, change);				
	}
}
