﻿using UnityEngine;
using System.Collections;

public class LocationLookTarget : MonoBehaviour {

	public int locationID = 0;

	private LocationData _location;
	public LocationData location {
		get { return _location; }
	}

	void OnEnable () {
		RequestManager.onLocationsRetrieved += OnLocationsRetrieved;
	}

	void OnDisable () {
		RequestManager.onLocationsRetrieved -= OnLocationsRetrieved;
	}

	void OnLocationsRetrieved (LocationsData locations) {
		if (locations == null || locations.locations == null) {
			return;
		}

		foreach (LocationData location in locations.locations) {
			if (location.id == locationID) {
				_location = location;
				break;
			}
		}
	}

}
