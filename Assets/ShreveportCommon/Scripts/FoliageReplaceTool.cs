using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class FoliageReplaceTool : MonoBehaviour
{
    public GameObject originalFoliageParent;
    public GameObject newFoliageParent;
    public GameObject foliagePrefab;

    // Update is called once per frame
    void Update()
    {
        if (!originalFoliageParent || !newFoliageParent || !foliagePrefab) return;

        if(newFoliageParent.transform.childCount > 0)
        {
            int i = 0;

            GameObject[] allChildren = new GameObject[newFoliageParent.transform.childCount];
            foreach(Transform child in newFoliageParent.transform)
            {
                allChildren[i] = child.gameObject;
                ++i;
            }

            foreach(GameObject child in allChildren)
            {
                DestroyImmediate(child.gameObject);
            }
        }

        for(int i = 0; i < originalFoliageParent.transform.childCount; i++)
        {
            Transform childTransform = originalFoliageParent.transform.GetChild(i);

            GameObject newFoliage = Instantiate(foliagePrefab);

            newFoliage.transform.parent = newFoliageParent.transform;
            newFoliage.transform.position = childTransform.position;
            newFoliage.transform.rotation = childTransform.rotation;
            newFoliage.transform.localScale = childTransform.localScale;

            newFoliage.name = foliagePrefab.name + "_" + i.ToString();
        }
    }
}
