﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Paraphernalia.Utils;

public class MenuController : MonoBehaviour {

	public static MenuController instance;
	public static bool isObstructingView {
		get {
			return (instance != null && (instance.visibility > 1 || instance.infoScreen.activeSelf));
		}
	}

	private RectTransform rect;
	public static float width {
		get { return instance.rect.sizeDelta.x; }
	}
	
	public static float height {
		get { return instance.rect.sizeDelta.y; }
	}

	public GameObject[] setActiveOnAwakeList;
	public GameObject infoScreen;

	public float maxVisibleDistance = 800;

	public Text locationText;
	public Text locationDetailText;
	public Text mapCameraToggleText;

	public string birdsEyeIcon;
	public string firstPersonIcon;

	public RectTransform menuBar;
	public RectTransform mediaScrollView;

	public GameObject mapCameraToggle;
	public GameObject loadingIndicator;
	public GameObject backButton;
	public GameObject locationWarning;

	private LocationData _currentLocation;
	public LocationData currentLocation {
		get { return _currentLocation; }
		set {
			if (value == null) {
				SetVisibility(0);
				_currentLocation = null;
				locationText.text = "";
				locationDetailText.text = "";
				loadingIndicator.SetActive(false);
			}
			else if (_currentLocation != value) {
				_currentLocation = value;
				SetVisibility(1);
				locationText.text = _currentLocation.title;
				locationDetailText.text = _currentLocation.info;
				MediaTab.DisableAllTabs();
				RequestManager.GetMediaPreviewsForLocation(_currentLocation.id);
				loadingIndicator.SetActive(true);
			}
		}
	}
	public float transitionTime = 0.25f;
	public Interpolate.EaseType easeType = Interpolate.EaseType.InOutQuad;
	private bool isTransitioning = false;

	private int visibility = 0;
	private int lastVisibility = 0;

	void Awake () {
		if (instance == null) {
			instance = this;
			rect = GetComponent<RectTransform>();
			foreach (GameObject g in setActiveOnAwakeList) {
				g.SetActive(true);
			}
		}
	}

	void Start () {
		currentLocation = null;
		mediaScrollView.localPosition = -Vector3.up * height + Vector3.right * mediaScrollView.anchoredPosition.x;
		mapCameraToggleText.text = firstPersonIcon;

		if (LocationManager.locationEnabled) {
			OnLocationServicesEnabled();
		}
		else {
			OnLocationServicesDisabled();
		}
	}

	void OnEnable () {
		MapCameraController.onCameraMoved += OnCameraMoved;
		RequestManager.onMediaPreviewsRetrieved += OnMediaPreviewsRetrieved;
		LocationManager.onServiceEnabled += OnLocationServicesEnabled;
		LocationManager.onServiceDisabled += OnLocationServicesDisabled;
	}

	void OnDisable () {
		MapCameraController.onCameraMoved -= OnCameraMoved;
		RequestManager.onMediaPreviewsRetrieved -= OnMediaPreviewsRetrieved;
		LocationManager.onServiceEnabled -= OnLocationServicesEnabled;
		LocationManager.onServiceDisabled -= OnLocationServicesDisabled;
	}

	void OnCameraMoved(float distance) {
		if (distance > maxVisibleDistance && visibility != 0) {
			currentLocation = null;
		}
	}

	void OnMediaPreviewsRetrieved (MediaData media) {
		loadingIndicator.SetActive(false);
	}
	
	public void ToggleCameraMode () {
		switch (MapCameraController.cameraMode) {
			case MapCameraController.CameraMode.FirstPerson:
				Input.gyro.enabled = false;
				mapCameraToggleText.text = firstPersonIcon;
				MapCameraController.SetBirdsEye();
				break;
			case MapCameraController.CameraMode.BirdsEye:
				if (LocationManager.locationEnabled) {
					Input.gyro.enabled = true;
					mapCameraToggleText.text = birdsEyeIcon;			
					MapCameraController.SetFirstPerson();
				}
				else {
					locationWarning.SetActive(true);
				}
				break;
		}
	}

	public void CollapseMenu () {
		SetVisibility(1);
		LocationsUIController.instance.FadeInLabels();
		MediaTab.DeselectAll();
	}

	public void ExpandMenu (string category) {
		SetVisibility(2);
		LocationsUIController.instance.FadeOutLabels();
		MediaListController.instance.ShowCategory(category);
	}

	public static void Transition (RectTransform fromRect, RectTransform toRect, int dir) {
		instance.StartCoroutine(instance.TransitionCoroutine(fromRect, toRect, dir));
	}

	IEnumerator TransitionCoroutine (RectTransform fromRect, RectTransform toRect, int dir) {
		if (!isTransitioning) {
	        isTransitioning = true;
	        float w = width * 2;
	        yield return new WaitForEndOfFrame();
	    	float t = 0;
	    	while (t < transitionTime) {
	    		t += Time.deltaTime;
	    		float frac = Interpolate.Ease(easeType, Mathf.Clamp01(t / transitionTime));
	            fromRect.anchoredPosition = dir * Vector3.right * Mathf.Lerp(0, w, frac) + Vector3.up * fromRect.anchoredPosition.y;
	            if (toRect != null) toRect.anchoredPosition = dir * Vector3.right * Mathf.Lerp(-w, 0, frac) + Vector3.up * toRect.anchoredPosition.y;
	    		yield return new WaitForEndOfFrame();
	    	}
	        isTransitioning = false;
	    }
    }

    public static void SetVisibility (int visibility) {
    	instance.StartCoroutine(instance.SetVisibilityCoroutine(visibility));
    }

    float VisibilityToPosition (int visibility) {
    	switch (visibility) {
    		case 0:
    			return height;
    		case 1:
    			return height - menuBar.rect.height - 10;
			default:
				return 0;
    	}
    }

	IEnumerator SetVisibilityCoroutine (int visibility) {
		if (!isTransitioning) {
			backButton.SetActive(visibility > 1);
	        isTransitioning = true;

	        lastVisibility = this.visibility;
	        this.visibility = visibility;

	        float a = VisibilityToPosition(lastVisibility);
	        float b = VisibilityToPosition(visibility);
	        yield return new WaitForEndOfFrame();
	    	float t = 0;
	    	while (t < transitionTime) {
	    		t += Time.deltaTime;
	    		float frac = Interpolate.Ease(easeType, Mathf.Clamp01(t / transitionTime));
	            mediaScrollView.anchoredPosition = -Vector3.up * Mathf.Lerp(a, b, frac) + Vector3.right * mediaScrollView.anchoredPosition.x;
	    		yield return new WaitForEndOfFrame();
	    	}

	    	bool showLocation = (
	    		LocationManager.locationEnabled && 
	    		visibility == 0 && 
	    		MapCameraController.cameraMode == MapCameraController.CameraMode.BirdsEye
	    	);

    		ClosestNotificationController.instance.gameObject.SetActive(showLocation);
    		UserLocationController.instance.gameObject.SetActive(showLocation);

	        isTransitioning = false;
	    }
    }

    public static bool TouchedUI () {
    	bool touchedUI = EventSystem.current.IsPointerOverGameObject();
    	foreach (Touch touch in Input.touches) {
    		touchedUI = EventSystem.current.IsPointerOverGameObject(touch.fingerId) || touchedUI;
    	}

    	return touchedUI;
    }

    void OnLocationServicesEnabled () {
    	bool showLocation = (visibility == 0 && MapCameraController.cameraMode == MapCameraController.CameraMode.BirdsEye);

		ClosestNotificationController.instance.gameObject.SetActive(showLocation);
		UserLocationController.instance.gameObject.SetActive(showLocation);

		mapCameraToggle.SetActive(true);
    }

    void OnLocationServicesDisabled () {
		ClosestNotificationController.instance.gameObject.SetActive(false);
		UserLocationController.instance.gameObject.SetActive(false);

		mapCameraToggle.SetActive(MapCameraController.cameraMode == MapCameraController.CameraMode.FirstPerson);
    }
}
