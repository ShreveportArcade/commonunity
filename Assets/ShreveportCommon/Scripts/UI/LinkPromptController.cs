﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LinkPromptController : MonoBehaviour {

	public static LinkPromptController instance;

	public Text titleText;
	public Text promptText;

	private string url;
	private string title;

	void Awake () {
		if (instance == null) {
			instance = this;
			gameObject.SetActive(false);
		}
	}

	public void ShowExternalURLPrompt (string url) {
		PromptExternalURL(url, url);
	}

    public static void PromptExternalURL (string url, string title) {
    	instance.url = url;
    	instance.title = title;

    	instance.titleText.text = "Are you sure?";
    	instance.promptText.text = "The link '" + title + "' will open in another application.";

    	instance.gameObject.SetActive(true);
    }

    public void OKSelected () {
    	RequestManager.OpenExternalURL(url, title);
    	instance.gameObject.SetActive(false);
    }
}
