﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Paraphernalia.Extensions;

public class LocationsUIController : MonoBehaviour {

	public static LocationsUIController instance;

	public LocationTextController labelPrefab;
	public LocationReferencePoint northEast;
	public LocationReferencePoint southWest;

	public int poolSize = 20;
	private List<LocationTextController> labels = new List<LocationTextController>();
	private double latDiff;
	private double lonDiff;
	private Vector3 posDiff;

	void Awake () {
		if (instance == null) {
			instance = this;
		}
	}
	
	void OnEnable () {
		RequestManager.onLocationsRetrieved += OnLocationsRetrieved;
		MapCameraController.onCameraMoved += UpdateLocationTextTransforms;
		UserLocationController.onUserMoved += UpdateLocationTextDistancesFromUser;
	}

	void OnDisable () {
		RequestManager.onLocationsRetrieved -= OnLocationsRetrieved;
		MapCameraController.onCameraMoved -= UpdateLocationTextTransforms;
		UserLocationController.onUserMoved -= UpdateLocationTextDistancesFromUser;
	}

	void Start () {
		GetLocations();
		latDiff = northEast.latitude - southWest.latitude;
		lonDiff = northEast.longitude - southWest.longitude;
		posDiff = northEast.transform.position - southWest.transform.position;
	}

	public void GetLocations () {
		RequestManager.GetLocations();
	}

	void WarmPool () {
		for (int i = 0; i < poolSize; i++) {
			LocationTextController label = labelPrefab.Instantiate() as LocationTextController;
			labels.Add(label);
			label.transform.SetParent(transform);
			label.transform.localScale = Vector3.one;
			gameObject.SetActive(false);
		}
	}

	void OnLocationsRetrieved (LocationsData locations) {
		if (locations == null || locations.locations == null) {
			return;
		}

		int iterations = Mathf.Max(labels.Count, locations.locations.Length);
		for (int i = 0; i < iterations; i++) {

			LocationTextController label;
			if (i < labels.Count) {
				label = labels[i];
			}
			else {
				label = labelPrefab.Instantiate() as LocationTextController;
				labels.Add(label);
				label.transform.SetParent(transform);
				label.transform.localScale = Vector3.one;
			}

			if (i < locations.locations.Length) {
				LocationData location = locations.locations[i];
				Vector3 p = LatLonToPosition(location.latitude, location.longitude);
				label.transform.position = p + Vector3.up * location.displayHeight;
				label.text.characterSize = location.displaySize;
				label.line.startWidth = location.displaySize;
				label.line.endWidth = 0;
				label.gameObject.SetActive(true);
				label.location = location;
			}
			else {
				label.gameObject.SetActive(false);
			}
		}
		UpdateLocationTextTransforms(Camera.main.transform.position.y);
	}

	public static Vector3 LatLonToPosition (double lat, double lon) {
		double xDiff = (double)instance.posDiff.x * (lon - instance.southWest.longitude) / instance.lonDiff;
		double zDiff = (double)instance.posDiff.z * (lat - instance.southWest.latitude) / instance.latDiff;
		return instance.southWest.transform.position + new Vector3((float)xDiff, 0, (float)zDiff);
				
	}

	public static double PositionToLat (Vector3 pos) {
		double latDiff = (double)instance.latDiff * (pos.z - instance.southWest.transform.position.z) / instance.posDiff.z;
		return instance.southWest.latitude + latDiff;
				
	}

	public static double PositionToLon (Vector3 pos) {
		double lonDiff = (double)instance.lonDiff * (pos.x - instance.southWest.transform.position.x) / instance.posDiff.x;
		return instance.southWest.longitude + lonDiff;
				
	}

	public static void HideLabels () {
		foreach (LocationTextController label in instance.labels) {
			label.gameObject.SetActive(false);
		}
	}

	public void FadeOutLabels () {
		foreach (LocationTextController label in labels) {
			label.FadeOut();
		}
	}

	public void FadeInLabels () {
		foreach (LocationTextController label in labels) {
			label.FadeIn();
		}
	}

	void UpdateLocationTextTransforms (float height) {
		if (MapCameraController.cameraMode == MapCameraController.CameraMode.BirdsEye) {
			foreach (LocationTextController label in labels) {
				if (label.location == null) {
					label.gameObject.SetActive(false);
					continue;
				}

				Vector3 v = Camera.main.WorldToViewportPoint(label.transform.position);
				if ((label.location.minHeight <= height + label.fadeDist && 
					height - label.fadeDist <= label.location.maxHeight && 
					v.x > -0.5f && v.x < 1.5f && v.y > -0.5f && v.y < 1.5f) ||
					(LocationData.closest != null && LocationData.closest == label.location)) {
					label.gameObject.SetActive(true);
					label.UpdateTransform(height);
				}
				else {
					label.gameObject.SetActive(false);
				}
			}
		}
	}

	public static void UpdateLocationTextDistancesFromUser () {
		if (MapCameraController.cameraMode == MapCameraController.CameraMode.BirdsEye) {
			foreach (LocationTextController label in instance.labels) {
				if (label.location == null) {
					label.gameObject.SetActive(false);
					continue;
				}

				label.UpdateText();
			}

			if (LocationData.closest != null) {
				ClosestNotificationController.UpdateNotification();
			}
		}
	}
}
