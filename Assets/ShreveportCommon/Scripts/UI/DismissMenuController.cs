﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class DismissMenuController : MonoBehaviour {

	public void OnMouseUpAsButton () {
		if (MenuController.TouchedUI()) return;
		MenuController.instance.currentLocation = null;
	}
}
