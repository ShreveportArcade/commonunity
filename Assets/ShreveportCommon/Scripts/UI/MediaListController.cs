﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Paraphernalia.Extensions;

[RequireComponent(typeof(ScrollRect))]
public class MediaListController : MonoBehaviour {

	public static MediaListController instance;

	public Mask mask;
	public MediaListItemController listItemPrefab;
	public RectTransform listScrollView;
	public RectTransform listContent;
	public RectTransform detailScrollView;
	public RectTransform detailContent;

	private List<MediumData> mediumList = new List<MediumData>();
	private ListLayoutGroup listLayoutGroup;
	private MediaData media;
	private MediumDetailController detailController;
	private ScrollRect scrollRect;
	private string category = "";

	void Awake () {
		if (instance == null) {
			instance = this;
			scrollRect = GetComponent<ScrollRect>();
			detailController = detailContent.GetComponent<MediumDetailController>();
			listLayoutGroup = listContent.GetComponent<ListLayoutGroup>();
			for (int i = 0; i < listLayoutGroup.visibleCount; i++) {
				AddListItem();
			}
		}
	}

	MediaListItemController AddListItem () {
		MediaListItemController listItem = listItemPrefab.Instantiate() as MediaListItemController;
		listItem.transform.SetParent(scrollRect.content);
		listItem.transform.localScale = Vector3.one;
		listItem.transform.localPosition = Vector3.zero;
		return listItem;
	}

	void Start () {
		mask.enabled = true;
		listContent.GetComponent<LayoutElement>().preferredWidth = MenuController.width;
		detailContent.GetComponent<LayoutElement>().preferredWidth = MenuController.width - 20;
        detailScrollView.localPosition = Vector3.right * MenuController.width * 2 + Vector3.up * detailScrollView.localPosition.y;
	}

	void OnEnable () {
		listLayoutGroup.onRowUpdate += OnRowUpdate;
		RequestManager.onMediaPreviewsRetrieved += OnMediaPreviewsRetrieved;
	}

	void OnDisable () {
		listLayoutGroup.onRowUpdate -= OnRowUpdate;
		RequestManager.onMediaPreviewsRetrieved -= OnMediaPreviewsRetrieved;
	}

	void OnMediaPreviewsRetrieved (MediaData media) {
		this.media = media;
		MediaTab.DisableAllTabs();
		if (this.media.media == null) return;

		foreach (string category in this.media.categorizedMedia.Keys) {
			MediaTab.EnableTab(category);
		}
		ShowCategory(this.category);
	}

	void OnRowUpdate(int row, int child) {
		MediaListItemController[] mediaListItems = listScrollView.GetComponentsInChildren<MediaListItemController>(true);
		MediaListItemController listItem = mediaListItems[child];
		if (row < mediumList.Count) {
			MediumData medium = mediumList[row];
			listItem.SetupWithMedium(medium);
		}
	}

	public static int MediaCount () {
		if (instance.media == null) return 0;
		return instance.media.media.Length;
	}

	public void ShowCategory (string name) {
		category = name;
		if (media == null) return;

		if (media.categorizedMedia.ContainsKey(category)) mediumList = media.categorizedMedia[category];
		else if (media.media != null && string.IsNullOrEmpty(category)) mediumList = new List<MediumData>(media.media);
		int mediaCount = (mediumList == null) ? 0 : mediumList.Count;
		listLayoutGroup.rows = mediaCount;
		
		MediaListItemController[] mediaListItems = listScrollView.GetComponentsInChildren<MediaListItemController>(true);
		for (int i = 0; i < mediaListItems.Length; i++) {
        	MediaListItemController listItem = mediaListItems[i];
			listItem.gameObject.SetActive(i < mediaCount);
		}

		if (mediaCount == 0) return;

        for (int i = 0; i < Mathf.Min(listLayoutGroup.visibleCount, mediaCount); i++) {
        	MediaListItemController listItem = null;
        	if (i >= mediaListItems.Length) listItem = AddListItem();
        	else listItem = mediaListItems[i];

        	if (i + listLayoutGroup.firstVisible < mediaCount) {		
				MediumData medium = mediumList[i + listLayoutGroup.firstVisible];
				listItem.SetupWithMedium(medium);
			}
		}
	}

	public void ShowDetail (MediumData medium) {
		MenuController.Transition(listScrollView, detailScrollView, -1);
		detailController.SetupWithMedium(medium);
		RequestManager.GetMedium(medium);
	}

	public void HideDetail () {
		MenuController.Transition(detailScrollView, listScrollView, 1);
		AudioPlaybackController.Stop();
	}
}
