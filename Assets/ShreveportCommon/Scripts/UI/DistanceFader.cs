﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DistanceFader : MonoBehaviour {

	public float minDist = 100;
	public float maxDist = 1000;
	public float fadeDist = 10;
	private Graphic graphic;
	private TextMesh text;
	private Color defaultColor;

	void OnEnable () {
		MapCameraController.onCameraMoved += OnCameraMoved;
	}

	void OnDisable () {
		MapCameraController.onCameraMoved -= OnCameraMoved;		
	}

	void Awake () {
		graphic = GetComponent<Graphic>();
		if (graphic != null) defaultColor = graphic.color;
		else {
			text = GetComponent<TextMesh>();
			defaultColor = text.color;
		}
	}

	void OnCameraMoved (float dist) {
		Color c = defaultColor;
		if (dist > minDist && dist < maxDist) {
			c.a = c.a * 1;
			if (graphic != null) graphic.raycastTarget = true;
		}
		else if (dist < minDist - fadeDist || dist > maxDist + fadeDist) {
			c.a = 0;
			if (graphic != null) graphic.raycastTarget = false;
		}
		else if (dist < minDist && dist > minDist - fadeDist) {
			c.a = c.a * (1 - (minDist - dist) / fadeDist);
		}
		else {
			c.a = c.a * (1 - (dist - maxDist) / fadeDist);
		}

		if (graphic != null) graphic.color = c;
		else text.color = c;
	}
}
