﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Paraphernalia.Utils;
using Paraphernalia.Extensions;

[RequireComponent(typeof(TextMesh))]
[RequireComponent(typeof(MeshRenderer))]
public class LocationTextController : MonoBehaviour {

	private LocationData _location;
	public LocationData location {
		get { return _location; }
		set {
			if (_location != value) {
				_location = value;
                UpdateText();
			}
		}
	}
	public float fadeDist = 10;
	public float fadeAlpha = 0.3f;

	private TextMesh _text;
	public TextMesh text {
		get { return _text; }
	}

	private LineRenderer _line;
	public LineRenderer line {
		get { return _line; }
	}

	private Color _initialColor;
	public Color initialColor {
		get { return _initialColor; }
	}

	void Awake () {
		_text = GetComponent<TextMesh>();
		_line = GetComponent<LineRenderer>();
		_initialColor = text.color;
	}

	public void OnMouseUpAsButton () {
		if (MenuController.TouchedUI()) return;
		MenuController.instance.currentLocation = location;
	}

	public void FadeOut () {
		Color c = _initialColor;
		c.a = fadeAlpha;
		text.color = c;
		line.startColor = c;
		line.endColor = c;
	}

	public void FadeIn () {
		text.color = _initialColor;
        line.startColor = _initialColor;
        line.endColor = _initialColor;
	}

	[Range(0,1)] public float warpAmount = 0.4f;
	[ContextMenu("FaceCamera")]
	public void FaceCamera () {
		transform.rotation = Quaternion.LookRotation(
			Vector3.Lerp(
				Camera.main.transform.forward, 
				(transform.position - Camera.main.transform.position).normalized, 
				warpAmount
			), 
			Camera.main.transform.up
		);
	}

	public void UpdateTransform (float height) {
		FaceCamera();

        Vector3 p = transform.position;
        line.SetPosition(0, p);
        p.y = 0;
        line.SetPosition(1, p);

        Color c = Color.clear;
        if (LocationData.closest != null && location.id == LocationData.closest.id) {
        	c = _initialColor;
        }
		else if (height <= location.minHeight) {
			float frac = 1 - (location.minHeight - height) / fadeDist;
			c = _initialColor * frac;
		}
		else if (height >= location.maxHeight) {
			float frac = 1 - (height - location.maxHeight) / fadeDist;
			c = _initialColor * frac;
		}
		else if (height > location.minHeight && height < location.maxHeight) {
			c = _initialColor;
		}

        text.color = c;
        line.startColor = c;
        line.endColor = c;
	}

    public void UpdateText() {
    	double d = location.MetersToUser();
        bool showDist = (d < location.maxAlertDist && location.hasDistAlerts && LocationManager.locationEnabled);
    	if (showDist) {
	        Color c = text.color;
	        float a = Mathf.Clamp01((float)d / location.maxAlertDist);
	        c.a *= (1 - a * a) * 0.8f + 0.2f;
	        text.text = string.Format("{0}\n<i><color=#{1}>{2} ft</color></i>   ", 
	            location.title, 
	            ColorUtils.RGBtoHex(c), 
	            Mathf.RoundToInt((float)location.FeetToUser())
	            );
	        if (4 * location.title.Length > text.text.Length) text.anchor = TextAnchor.MiddleCenter;
	        else text.anchor = TextAnchor.LowerCenter;
	    }
	    else {
	    	text.text = location.title;
	    	text.anchor = TextAnchor.LowerCenter;
	    }

    	float width = 0;
    	float height = 0;
    	float maxY = -99999999;
	    foreach (char symbol in location.title) {
	        CharacterInfo info;
	        if (text.font.GetCharacterInfo(symbol, out info, text.fontSize, text.fontStyle))
	        {
	            width += info.advance;
	            height = Mathf.Max(height, info.maxY - info.minY);
	            if (info.maxY > maxY) maxY = info.maxY;
	        }
	    }
	    if (showDist) {
	    	maxY *= 2;
	    	height *= 2;
	    }
        BoxCollider box = GetComponent<BoxCollider>();
        if (text.anchor == TextAnchor.LowerCenter) box.center = Vector3.up * maxY * text.characterSize * 0.1f;
        else box.center = Vector3.zero;
		box.size = new Vector3(width, height, 1) * text.characterSize * 0.1f;
    }
}
