﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(AudioSource))]
public class AudioPlaybackController : MonoBehaviour {

	public static AudioPlaybackController instance;

	public string playButtonIcon;
	public string pauseButtonIcon;
	public GameObject loadingIndicator;
	public Text playPauseButtonText;
	public Slider trackingSlider;
	public Text playbackTimeText;

	new private AudioSource audio;
	private bool shouldPlay = false;

	void Awake () {
		if (instance == null) {
			instance = this;
			audio = GetComponent<AudioSource>();
		}
	}

	void OnEnable () {
		trackingSlider.value = 0;
		playbackTimeText.text = "-:--";
		RequestManager.onAudioRetrieved += OnAudioRetrieved;
	}

	void OnDisable () {
		RequestManager.onAudioRetrieved -= OnAudioRetrieved; 
		Stop();
	}

	public void PlayPause () {
		StopCoroutine("PlayCoroutine");
		if (audio.isPlaying) {
			shouldPlay = false;
			playPauseButtonText.text = playButtonIcon;
			audio.Pause();
		}
		else {
			shouldPlay = true;
			playPauseButtonText.text = pauseButtonIcon;
			StartCoroutine("PlayCoroutine");
		}
	}

	public static void Stop () {
		instance.shouldPlay = false;
		instance.playPauseButtonText.text = instance.playButtonIcon;
		instance.audio.Stop();
		instance.audio.clip = null;
	}

	public void SetTracking () {
		StopCoroutine("PlayCoroutine");
		if (audio.isPlaying) audio.Pause();
		if (audio.clip != null) {
			audio.time = audio.clip.length * trackingSlider.value;
			UpdateTimeText();
			if (shouldPlay) StartCoroutine("PlayCoroutine");
		}
	}

	public void SetAudioURL(string url) {
		loadingIndicator.SetActive(true);
		playPauseButtonText.gameObject.SetActive(false);
        RequestManager.GetAudio(url);
	}

	void OnAudioRetrieved (AudioClip clip) {
		audio.clip = clip;
		loadingIndicator.SetActive(false);
		playPauseButtonText.gameObject.SetActive(true);
		playPauseButtonText.text = playButtonIcon;
		UpdateTimeText();
	}

	IEnumerator PlayCoroutine () {
		while (audio.clip == null || audio.clip.loadState != AudioDataLoadState.Loaded) {
            yield return new WaitForEndOfFrame();
		}
		audio.Play();
		while (shouldPlay && audio.isPlaying) {
            yield return new WaitForEndOfFrame();
            if (audio.clip != null) {
            	trackingSlider.value = audio.time / audio.clip.length;
	            UpdateTimeText();
	        }
		}
		if (!shouldPlay) audio.Pause();
	}

	void UpdateTimeText () {
		int minutes = (int)audio.time / 60;
		int seconds = (int)audio.time % 60;
		playbackTimeText.text = string.Format("{0}:{1:D2}", minutes, seconds);
	}
}
