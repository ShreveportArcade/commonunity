﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Paraphernalia.Extensions;

public class MediumDetailController : MonoBehaviour {

	public Text titleText;
	public Text detailText;
	public Text bodyText;
	public RawImage image;
	public GameObject videoIcon;
	public GameObject documentIcon;
	public GameObject expandIcon;
	public LinkController linkControllerPrefab;
	public GameObject linksRoot;
	public GameObject bodyRoot;
	public AudioPlaybackController audioController;
	public FullscreenImageController fullscreenImageController;

	public MediumData medium;
	private Color initialColor;
	
	void Start () {
		initialColor = image.color;
	}

	void OnEnable () {
		RequestManager.onMediumRetrieved += SetupWithMedium;
	}

	void OnDisable () {
		RequestManager.onMediumRetrieved -= SetupWithMedium;
	}

	public void SetupWithMedium (MediumData medium) {
		this.medium = medium;
		titleText.text = medium.title;
		detailText.text = medium.creator;
		bodyText.text = medium.text;
		bodyRoot.SetActive(!string.IsNullOrEmpty(medium.text));
		image.texture = medium.thumbnailTex;
		image.color = (image.texture == null) ? initialColor : Color.white;
		videoIcon.SetActive(!string.IsNullOrEmpty(medium.videoURL));
		documentIcon.SetActive(!string.IsNullOrEmpty(medium.documentURL));
		expandIcon.SetActive(!string.IsNullOrEmpty(medium.lowResImageURL));

		if (!string.IsNullOrEmpty(medium.headerURL)) RequestManager.GetPhoto(medium.headerURL, image);
		if (string.IsNullOrEmpty(medium.audioURL)) {
			audioController.gameObject.SetActive(false);
		}
		else {
			audioController.gameObject.SetActive(true);
			audioController.SetAudioURL(medium.audioURL);
		}
		
		LayoutElement layout = image.GetComponent<LayoutElement>();
		if (layout != null) {
        	RectTransform rect = image.GetComponent<RectTransform>();
            layout.preferredHeight = (float)rect.rect.width * 9f / 16f;
		}

		LinkController[] linkControllers = linksRoot.gameObject.GetComponentsInChildren<LinkController>(true);
		int linkCount = ((medium.links == null)?0:medium.links.Length);
		if (linkCount == 0) {
			linksRoot.SetActive(false);
		}
		else {
			linksRoot.SetActive(true);
			for (int i = 0; i < Mathf.Max(linkCount, linkControllers.Length); i++) {
				LinkController linkController;
				if (i < linkControllers.Length) linkController = linkControllers[i];
				else linkController = linkControllerPrefab.Instantiate() as LinkController;
				
				if (i < linkCount) {
					LinkData link = medium.links[i];
					linkController.gameObject.SetActive(true);
					linkController.SetupWithLink(link);
					linkController.transform.SetParent(linksRoot.transform);
					linkController.transform.localScale = Vector3.one;
					linkController.transform.localPosition = Vector3.zero;
				}
				else {
					linkController.gameObject.SetActive(false);
				}
			}
		}
	}

	public void VideoButtonPressed () {
		LinkPromptController.PromptExternalURL(medium.videoURL, medium.title);
	}

	public void DocumentButton () {
		LinkPromptController.PromptExternalURL(medium.documentURL, medium.title);
	}

	public void ExpandButtonPressed () {
		if (medium.lowResImageURL != null) {
			fullscreenImageController.gameObject.SetActive(true);
			fullscreenImageController.SetupWithMedium(medium);
		}		
	}
}
