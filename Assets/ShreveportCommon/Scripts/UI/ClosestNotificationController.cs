﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClosestNotificationController : MonoBehaviour {

	public static ClosestNotificationController instance;

	public Text locationText;

	void Awake () {
		if (instance == null) instance = this;
	}

	void OnEnable () {
		LocationData.onAlertChanged += UpdateNotification;
		LocationData.onClosestChanged += UpdateNotification;
		UpdateNotification();
	}

	void OnDisable () {
		LocationData.onAlertChanged -= UpdateNotification;
		LocationData.onClosestChanged -= UpdateNotification;
	}

	public static void UpdateNotification () {
		if (LocationData.closest == null) {
			instance.locationText.text = "";
			LocationsUIController.UpdateLocationTextDistancesFromUser();
			return;
		}

		if (LocationData.closest.FeetToUser() > 2000 ) {
			float miles = (float)LocationData.closest.FeetToUser() / 5280f;
			string word = (miles == 1) ? "mile" : "miles";
			instance.locationText.text = string.Format("You're <b>{0:0.0#} {1}</b> from <b>Shreveport Common</b>", miles, word);
		}
		else if (LocationData.closestAlert == null) {
			instance.locationText.text = "No nearby attractions found.";
		}
		else {
			LocationData c = LocationData.closestAlert;
			int feet = Mathf.RoundToInt((float)c.FeetToUser());
			string word = (feet == 1) ? " foot" : " feet";
			instance.locationText.text = string.Format("You're <b>{0} {1}</b> from <b>{2}</b>", feet, word, c.title);
		}
	}

	public void ZoomIn () {
		if (LocationData.closestAlert == null) return;
		MenuController.instance.currentLocation = LocationData.closestAlert;
		MapCameraController.ZoomInOnUser();
	}
}
