﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class MediaTab : MonoBehaviour {

	public static List<MediaTab> tabs = new List<MediaTab>();

	public string category = "";
	public Image backgroundImage;
	public Text[] foregroundTexts;
	public Image[] foregroundImages;
	public Color selectedBackgroundColor = Color.black;
	public Color selectedForegroundColor = Color.white;

	private Color normalBackgroundColor;
	private Color normalForegroundColor;

	void Awake () {
		tabs.Add(this);
	}

	void Start () {
		normalBackgroundColor = backgroundImage.color;
		normalForegroundColor = foregroundTexts[0].color;
	}

	public void Select () {
		if (MediaListController.MediaCount() == 0) return;

		backgroundImage.color = selectedBackgroundColor;
		foreach (Text text in foregroundTexts) {
			text.color = selectedForegroundColor;
		}
		foreach (Image image in foregroundImages) {
			image.color = selectedForegroundColor;
		}

		MenuController.instance.ExpandMenu(category);
		foreach (MediaTab tab in tabs) {
			if (tab != this) {
				tab.Deselect();
			}
		}
	}

	public void Deselect () {
		backgroundImage.color = normalBackgroundColor;
		foreach (Text text in foregroundTexts) {
			text.color = normalForegroundColor;
		}
		foreach (Image image in foregroundImages) {
			image.color = normalForegroundColor;
		}
	}

	public static void DeselectAll () {
		foreach (MediaTab tab in tabs) tab.Deselect();
	}

	public static void DisableAllTabs () {
		foreach (MediaTab tab in tabs) {
			if (tab.category != "") tab.gameObject.SetActive(false);
		}
	}

	public static void EnableTab (string category) {
		foreach (MediaTab tab in tabs) {
			if (category == tab.category) tab.gameObject.SetActive(true);
		}
	}
}
