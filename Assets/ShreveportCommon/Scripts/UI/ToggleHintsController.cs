﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ToggleHintsController : MonoBehaviour {

	public Text buttonText;

	public GameObject[] hints;

	IEnumerator Start () {
		yield return new WaitForEndOfFrame();
		bool showHints = PlayerPrefs.GetInt("ShowHints", 1) == 1;
		SetVisibility(showHints);
	}

	public void ToggleHints () {
		bool showHints = PlayerPrefs.GetInt("ShowHints", 1) == 1;
		showHints = !showHints;
		PlayerPrefs.SetInt("ShowHints", showHints ? 1 : 0);
		SetVisibility(showHints);
	}
	
	void SetVisibility (bool showHints) {
		buttonText.text = showHints ? "Hide Hints" : "Show Hints";
		foreach (GameObject hint in hints) {
			hint.SetActive(showHints);
		}
	}
}
