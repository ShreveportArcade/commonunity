﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LinkController : MonoBehaviour {

	public LinkData link;
	public Text titleText;
	public Text urlText;

	public void SetupWithLink (LinkData link) {
		this.link = link;
		titleText.text = link.title;
		urlText.text = link.url;
	}

	public void OnLinkClicked () {
		LinkPromptController.PromptExternalURL(link.url, link.title);
	}
}
