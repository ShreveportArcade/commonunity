﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MediaListItemController : MonoBehaviour {

	public Text titleText;
	public Text creatorText;
	public Text previewText;
	public RawImage image;
	public GridLayoutGroup gridGroup;

	private MediumData medium;
	private RectTransform rect;

	void Awake () {
		rect = GetComponent<RectTransform>();
	}

	public void OnEnable () {
		RequestManager.onThumbnailUpdated += OnThumbnailUpdated;
	}

	public void OnDisable () {
		RequestManager.onThumbnailUpdated -= OnThumbnailUpdated;
	}

	public void SetupWithMedium (MediumData medium) {
		this.medium = medium;
		titleText.text = medium.title.ToUpper();
		creatorText.text = medium.creator;
		previewText.text = medium.textPreview;
		RequestManager.GetThumbnail(medium);
		gridGroup.cellSize = new Vector2(rect.rect.width * 0.5f, rect.rect.height);
	}

	public void ShowMedium () {
		MediaListController.instance.ShowDetail(medium);
	}

	public void OnThumbnailUpdated (MediumData medium) {
		if (medium != this.medium) return;

		image.texture = medium.thumbnailTex;
		AspectRatioFitter aspect = image.GetComponent<AspectRatioFitter>();
        if (aspect != null && image.texture != null) {
            aspect.aspectRatio = (float)image.texture.width / (float)image.texture.height;
        }
	}
}
