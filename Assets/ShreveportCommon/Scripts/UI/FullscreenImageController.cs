﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class FullscreenImageController : MonoBehaviour {

	public RawImage image;
	public GameObject[] uiOverlay;

	public void SetupWithMedium (MediumData medium) {
		RequestManager.GetFullscreen(medium, image);
	}
}
