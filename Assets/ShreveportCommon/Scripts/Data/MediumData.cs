using UnityEngine;

[System.Serializable]
public class MediumData {
	
	public int id;
	public string title;
	public string category;
	public string creator;

	public string text;
	public string textPreview;

	public string thumbnailURL;
	public string headerURL;
	public string lowResImageURL;
	public string highResImageURL;

	public string videoURL;
	public string documentURL;

	public string desktopAudioURL;
	public string mobileAudioURL;
	public string webAudioURL;
	public string audioURL {
		get {
			#if UNITY_STANDALONE
			return desktopAudioURL;
			#elif UNITY_WEBGL
			return webAudioURL;
			#else
			return mobileAudioURL;
			#endif
		}
	}

	public Texture2D thumbnailTex;

	public LinkData[] links;

	public static MediumData CreateFromJSON(string jsonString) {
		return JsonUtility.FromJson<MediumData>(jsonString);
	}

	public static void UpdateFromJSON(string jsonString, MediumData medium) {
		JsonUtility.FromJsonOverwrite(jsonString, medium);
	}
}