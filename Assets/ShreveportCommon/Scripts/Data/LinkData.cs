using UnityEngine;

[System.Serializable]
public class LinkData {
	
	public int id;
	public string title;
	public string url;
}