using UnityEngine;
using System.Collections.Generic;

[System.Serializable]
public class MediaData {
	
	public MediumData[] media;

	private Dictionary<string, List<MediumData>> _categorizedMedia;
	public Dictionary<string, List<MediumData>> categorizedMedia {
		get { return _categorizedMedia; }
	}

	public static MediaData CreateFromJSON(string jsonString) {
		MediaData m = JsonUtility.FromJson<MediaData>(jsonString);
		Dictionary<string, List<MediumData>> categorizedMedia = new Dictionary<string, List<MediumData>>();
		if (m == null || m.media == null) {
			m = new MediaData();
		}
		else {
			foreach (MediumData medium in m.media) {
				if (!categorizedMedia.ContainsKey(medium.category)) {
					categorizedMedia[medium.category] = new List<MediumData>();
				}
				categorizedMedia[medium.category].Add(medium);
			}
			m._categorizedMedia = categorizedMedia;
		}
		m._categorizedMedia = categorizedMedia;
		return m;
	}
}