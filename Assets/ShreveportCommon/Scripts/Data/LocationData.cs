using UnityEngine;
using System;

[System.Serializable]
public class LocationData {
	
	public int id;
	public bool hasDistAlerts;
	public float maxAlertDist;
	public float minHeight;
	public float maxHeight;
	public float displayHeight;
	public float displaySize;
	public string title;
	public string info;
	public string category;
	public double latitude;
	public double longitude;

	public delegate void OnClosestChanged ();
	public static OnClosestChanged onClosestChanged = delegate {};
	public static OnClosestChanged onAlertChanged = delegate {};

	private static LocationData _closest = null;
	public static LocationData closest {
		get { return _closest; }
		set {
			if (_closest != value) {
				_closest = value;
				onClosestChanged();
			}
		}
	}

	private static LocationData _closestAlert = null;
	public static LocationData closestAlert {
		get { return _closestAlert; }
		set {
			if (_closestAlert != value) {
				_closestAlert = value;
				onAlertChanged();
			}
		}
	}

	private double metersToUser = -1;
	public double MetersToUser() {
		if (LocationManager.instance == null) return -1;
        double dLat = (LocationManager.lat - this.latitude) * Math.PI / 180.0;
        double dLon = (LocationManager.lon - this.longitude) * Math.PI / 180.0;
        double a = Math.Sin(dLat / 2) * Math.Sin(dLat / 2) + 
        		   Math.Cos(this.latitude * Math.PI / 180.0) * 
        		   Math.Cos(LocationManager.lat * Math.PI / 180.0) * Math.Sin(dLon / 2) * 
        		   Math.Sin(dLon / 2);
        double c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));
        metersToUser = c * 6371008.8; // rad of Earth in m
        if (hasDistAlerts && metersToUser < maxAlertDist && (closestAlert == null || metersToUser < closestAlert.metersToUser)) closestAlert = this;
        if (closestAlert == this && metersToUser > maxAlertDist) closestAlert = null;
        if (closest == null || metersToUser < closest.metersToUser) closest = this;
        return metersToUser;
	}

	public double FeetToUser() {
		return metersToUser * 3.28084;
	}

}