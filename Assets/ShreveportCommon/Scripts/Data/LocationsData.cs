using UnityEngine;

[System.Serializable]
public class LocationsData {
	
	public LocationData[] locations;

	public static LocationsData CreateFromJSON(string jsonString) {
		return JsonUtility.FromJson<LocationsData>(jsonString);
	}
}