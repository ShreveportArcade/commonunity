﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using MiniJSON;

public class RequestManager : MonoBehaviour {

	public const string baseAPIURL = "http://commonlink.sharca.de/";
    
    private static int _requests = 0;
    public static int requests {
        get { return _requests; }
        set {
            #if !UNITY_WEBGL
            if (value > 0 && _requests == 0) Handheld.StartActivityIndicator();
            else if (value == 0 && _requests > 0) Handheld.StopActivityIndicator();
            #endif
            
            _requests = value;
        }
    }

	public static RequestManager instance;

    public delegate void OnUserLocationRetrieved (double latitude, double longitude);
    public static event OnUserLocationRetrieved onUserLocationRetrieved = delegate {};

	public delegate void OnLocationsRetrieved (LocationsData locations);
	public static event OnLocationsRetrieved onLocationsRetrieved = delegate {};

	public delegate void OnMediaPreviewsRetrieved (MediaData media);
	public static event OnMediaPreviewsRetrieved onMediaPreviewsRetrieved = delegate {};

    public delegate void OnMediumUpdated (MediumData medium);
    public static event OnMediumUpdated onMediumRetrieved = delegate {};
    public static event OnMediumUpdated onThumbnailUpdated = delegate {};

    public delegate void OnAudioRetrieved (AudioClip clip);
    public static event OnAudioRetrieved onAudioRetrieved = delegate {};

	void Awake () {
		if (instance == null) {
			instance = this;
		}
	}

    public void ClearCache () {
        System.IO.DirectoryInfo di = new DirectoryInfo(Application.persistentDataPath);
        foreach (FileInfo file in di.GetFiles()) {
            file.Delete();
        }
    }

	public static WWW GetJSON (string route, int daysToExpire) {
        string url = baseAPIURL + route + ".json";
		return GetAsset(url, daysToExpire);
	}

    public static WWW GetAsset (string route, int daysToExpire) {
        string url = route;
        #if !UNITY_WEBGL || UNITY_EDITOR
            if (url.StartsWith("//")) url = "https:" + url;
        #endif

        string extension = Path.HasExtension(url) ? "." + Path.GetExtension(url) : "";
        string filePath = Application.persistentDataPath + "/" + route.GetHashCode() + extension;

        if (File.Exists(filePath) && (DateTime.Now - File.GetLastWriteTime(filePath)).Days > daysToExpire) {
            Debug.Log(filePath);
            Debug.Log((DateTime.Now - File.GetLastWriteTime(filePath)).Days + " days old");
            return new WWW("file://" + filePath);
        }
        else {
            return new WWW(url);
        }        
    }

    public static void CacheJSON (WWW www, string route) {
        CacheAsset(www, baseAPIURL + route + ".json");
    }

    public static void CacheAsset (WWW www, string route) {
        if (!www.url.StartsWith("file://") && www.error == null) {
            string extension = Path.HasExtension(www.url) ? "." + Path.GetExtension(www.url) : "";
            string filePath = Application.persistentDataPath + "/" + route.GetHashCode() + extension;
            File.WriteAllBytes(filePath, www.bytes);
        }
    }

	public static void GetLocations () {
		instance.StartCoroutine(instance.GetLocationsCoroutine());
	}

    public static void GetMediaPreviewsForLocation (int locationID) {
    	instance.StartCoroutine(instance.GetMediaPreviewsForLocationCoroutine(locationID));
    }

    public static void GetMedium (MediumData medium) {
    	instance.StartCoroutine(instance.GetMediumCoroutine(medium));
    }

    public static void GetPhoto (string route, RawImage image) {
        instance.StartCoroutine(instance.GetPhotoCoroutine(route, image));
    }
    
    public static void GetThumbnail (MediumData medium) {
        instance.StartCoroutine(instance.GetThumbnailCoroutine(medium));
    }

    public static void GetFullscreen (MediumData medium, RawImage image) {
        instance.StartCoroutine(instance.GetFullscreenCoroutine(medium, image));
    }

    public static void GetAudio (string route) {
    	instance.StartCoroutine(instance.GetAudioCoroutine(route));
    }

    public static void OpenExternalURL (string url, string title) {
        if (string.IsNullOrEmpty(url)) return;
         
        #if !UNITY_WEBGL || UNITY_EDITOR
            if (url.StartsWith("//")) url = "https:" + url;
            Application.OpenURL(url);
        #else
            Application.ExternalEval("window.open('" + url + "','" + title + "')");
        #endif
    }

    public static void GetUserLocationFromIP() {
        instance.StartCoroutine(instance.GetUserLocationCoroutine());
    }

    IEnumerator GetUserLocationCoroutine () {
        string url = "freegeoip.net/json/";
        requests++;
        WWW www = GetAsset(url, 0);
        yield return www;
        Dictionary<string,object> dict = Json.Deserialize(www.text) as Dictionary<string,object>;
        onUserLocationRetrieved((double)dict["latitude"], (double)dict["longitude"]);
        CacheAsset(www, url);
        requests--;
    }

    IEnumerator GetLocationsCoroutine () {
        requests++;
    	WWW www = GetJSON("/v1/locations", 0);
    	yield return www;
        onLocationsRetrieved(LocationsData.CreateFromJSON(www.text));
        CacheJSON(www, "/v1/locations");
        requests--;
    }
    
	IEnumerator GetMediaPreviewsForLocationCoroutine (int locationID) {
        requests++;
    	WWW www = GetJSON("/v1/locations/" + locationID + "/media", 7);
    	yield return www;
    	onMediaPreviewsRetrieved(MediaData.CreateFromJSON(www.text));
        CacheJSON(www, "/v1/locations/" + locationID + "/media");
        requests--;
    }
    
	IEnumerator GetMediumCoroutine (MediumData medium) {
        requests++;
    	WWW www = GetJSON("/v1/media/" + medium.id, 30);
    	yield return www;
        MediumData.UpdateFromJSON(www.text, medium);
    	onMediumRetrieved(medium);
        CacheJSON(www, "/v1/media/" + medium.id);
        requests--;
    }
    
	IEnumerator GetPhotoCoroutine (string route, RawImage image) {
        requests++;
    	WWW www = GetAsset(route, 30);
    	yield return www;
    	image.texture = www.texture;
        image.color = Color.white;
        CacheAsset(www, route);
        requests--;
    }

    IEnumerator GetThumbnailCoroutine (MediumData medium) {
        if (string.IsNullOrEmpty(medium.thumbnailURL)) {
            medium.thumbnailTex = null;
            yield return new WaitForEndOfFrame();
        }
        else {
            requests++;
            WWW www = GetAsset(medium.thumbnailURL, 30);
            yield return www;
            medium.thumbnailTex = www.texture;
            CacheAsset(www, medium.thumbnailURL);
            requests--;
        }
        onThumbnailUpdated(medium);
    }

    IEnumerator GetFullscreenCoroutine (MediumData medium, RawImage image) {
        requests++;
        WWW www = GetAsset(medium.lowResImageURL, 30);
        yield return www;
        image.texture = www.texture;
        CacheAsset(www, medium.lowResImageURL);

        AspectRatioFitter aspect = image.GetComponent<AspectRatioFitter>();
        if (aspect != null) {
            aspect.aspectRatio = (float)image.texture.width / (float)image.texture.height;
        }

        www = GetAsset(medium.highResImageURL, 30);
        yield return www;
        image.texture = www.texture;
        CacheAsset(www, medium.highResImageURL);
        requests--;
    }

    IEnumerator GetAudioCoroutine (string route) {
        requests++;
    	WWW www = GetAsset(route, 30);
    	yield return www;
        onAudioRetrieved(www.GetAudioClip(false));
        CacheAsset(www, route);
        requests--;
    }
}
