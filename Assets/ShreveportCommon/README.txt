ABOUT:
    The Shreveport Common App is an open source, community driven tour of the history, architecture, culture, and arts of a 9-block historic district in Downtown Shreveport, Louisiana.

SUBMIT CHECKLIST:
    bump version number
    turn off debug mode (logs, fake data, etc.)
    enable location in info.plist & manifest.xml
        NSLocationWhenInUseUsageDescription: Location is used to show nearby attractions in the Shreveport Common area. Your location is not sent or stored.

TODO:
    finish modeling/texturing buildings
    properly handle location text when too close
    properly handle clusters of location text
    write out initial setup and deploy guide for web app
    swipe to next or previous photos
    pinch zoom on detail photos
    zoom shouldn't rotate, rotate shouldn't zoom 
    web app option for cropping vs fitting
    
BUGS:
    list items reused too early
    audio not working on desktop platforms
    memory use too high, crashes on iPad2 and other devices with 256MB RAM
    mp3s sent from chrome have audio/mp3 MIME-type instead of audio/mpeg and get rejected
    mp3s are transcoded with incorrect length information, Unity plays these wrong
        hacky fix is to just serve original mp3

ICONS:
    Categories
        History       fa-clock-o [&#xf017;]
        Art           fa-photo (alias) [&#xf03e;]
        Stories       fa-book [&#xf02d;]
        Music         fa-music [&#xf001;]
        Links         fa-link [&#xf0c1;]
        Murals        fa-paint-brush [&#xf1fc;]
   
    Audio   
        Play          fa-play [&#xf04b;]
        Pause         fa-pause [&#xf04c;]

    Menu Navigation
        Show Image    fa-expand [&#xf065;]
        Hide Image    fa-compress [&#xf066;]
        Open PDF      fa-file-text [&#xf15c;]
        Back          fa-chevron-circle-left [&#xf137;]
        Close         fa-close (alias) [&#xf00d;]

    Network
        Loading       fa-spinner [&#xf110;]
        Refresh       fa-refresh [&#xf021;]

    Info Screen
        Info          fa-info-circle [&#xf05a;]
        Menu          fa-bars [&#xf0c9;] 
        Settings      fa-gear (alias) [&#xf013;]
        Credits       fa-hand-o-right [&#xf0a4;]

    Map
        First Person  fa-eye [&#xf06e;]
        Bird's Eye    fa-map-o [&#xf278;]
        Closest       fa-map-marker [&#xf041;] 
